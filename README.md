# Lage

Logging made simple

```go
l := lage.NewDefault()
defer l.Close()

l.Log(lage.Info, "A simple log message")
l.Logf(lage.Error, "You rolled a %d", 5)
```

## Children Logger

```go
l := lage.NewDefault()
l.Name = "A"
defer l.Close()

l2 := &lage.Logger{Name: "B"}
l2.SetParent(l)

l2.Log(lage.Info, "A simple log message")
```

## Level

Create your own log level

```go
lvl := &lage.Level{
    ID:    "apocalypse",
    Name:  "apocalypse",
    Color: lage.BackgroundRed,
}

l.Log(lvl, "We're all gonna die!")
```

## Config

Simply store a logger in your yaml config

```go
type Config struct {
  Logger *lage.Logger
}

func (c *Config) Load(name string) error {
	b, err := os.ReadFile(name)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}

	return yaml.Unmarshal(b, c)
}

func (c *Config) Save(name string) error {
	b, err := yaml.Marshal(c)
	if err != nil {
		return err
	}

	return os.WriteFile(name, b, 0600)
}

func main() {
    c := Config{}
    c.Load("config.yaml")
    defer c.Save("config.yaml")
}
```
