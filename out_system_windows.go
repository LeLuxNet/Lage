//go:build windows
// +build windows

package lage

func NewSystem() System {
	return System{}
}

func (o System) Log(m Message) {}

func (o System) Close() {}
