package lage

import (
	"regexp"
	"strings"
	"sync"
	"time"
)

type Message struct {
	Level   *Level
	Names   []string
	Content string

	contentNoANSILock sync.Mutex
	contentNoANSI     string

	Time time.Time
}

var timeFormat = "2006-01-02 15:04:05.000"

func (m *Message) lenJoinedNames() int {
	if len(m.Names) == 0 {
		return 0
	}

	i := 2 + 2 - 1
	for _, n := range m.Names {
		i += len(n) + 1
	}
	return i
}

func (m *Message) writeJoinedNames(b *strings.Builder) {
	if len(m.Names) == 0 {
		return
	}

	b.WriteByte('[')
	b.WriteString(m.Names[0])
	for _, n := range m.Names {
		b.WriteByte('/')
		b.WriteString(n)
	}
	b.WriteString("] ")
}

var ansiRe = regexp.MustCompile(`\x1b\[[0-9;]*m`)

func (m *Message) filterANSI() {
	if m.Content == "" || m.contentNoANSI != "" {
		return
	}

	m.contentNoANSILock.Lock()
	defer m.contentNoANSILock.Unlock()

	if m.Content == "" || m.contentNoANSI != "" {
		return
	}

	m.contentNoANSI = ansiRe.ReplaceAllString(m.Content, "")
}

func (m *Message) String() string {
	m.filterANSI()
	return m.string(false)
}

func (m *Message) StringANSI() string {
	return m.string(true)
}

func (m *Message) string(color bool) string {
	i := len(timeFormat) + 1 +
		m.lenJoinedNames() +
		len(m.Level.Name) + 2
	if color {
		i += ansiLen(FBrBlack) +
			ansiLen(Reset) +
			ansiLen(m.Level.Colors...) +
			ansiLen(Reset) + len(m.Content)
	} else {
		i += len(m.contentNoANSI)
	}

	var b strings.Builder
	b.Grow(i)

	if color {
		ansiWrite(&b, FBrBlack)
	}
	b.WriteString(m.Time.UTC().Format(timeFormat))
	if color {
		ansiWrite(&b, Reset)
	}
	b.WriteByte(' ')

	m.writeJoinedNames(&b)

	if color {
		ansiWrite(&b, m.Level.Colors...)
	}
	b.WriteString(m.Level.Name)
	if color {
		ansiWrite(&b, Reset)
	}
	b.WriteString(": ")

	if color {
		b.WriteString(m.Content)
	} else {
		b.WriteString(m.contentNoANSI)
	}

	return b.String()
}
