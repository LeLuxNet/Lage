package lage

type Level struct {
	ID     string
	Name   string
	Colors []Color

	SyslogLevel SyslogLevel
}

var (
	Debug = &Level{
		ID:          "debug",
		Name:        "debug",
		Colors:      []Color{FBrCyan},
		SyslogLevel: SyslogDebug,
	}

	Info = &Level{
		ID:          "info",
		Name:        "info",
		Colors:      []Color{FBrBlue},
		SyslogLevel: SyslogInfo,
	}

	Warn = &Level{
		ID:          "warning",
		Name:        "warn",
		Colors:      []Color{FYellow},
		SyslogLevel: SyslogWarning,
	}

	Error = &Level{
		ID:          "error",
		Name:        "error",
		Colors:      []Color{FBrRed},
		SyslogLevel: SyslogError,
	}

	Fatal = &Level{
		ID:          "fatal",
		Name:        "fatal",
		Colors:      []Color{BRed, FBrWhite},
		SyslogLevel: SyslogCritical,
	}
)
