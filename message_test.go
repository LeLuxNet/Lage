package lage_test

import (
	"testing"

	"lelux.net/lage"
)

func TestANSIRemoval(t *testing.T) {
	m := lage.Message{
		Level:   lage.Info,
		Content: "abc",
	}
	mStr := m.String()

	m.Content = lage.ANSI(lage.FBlue) + m.Content

	if m.String() != mStr {
		t.Fatalf("Expected ANSI to be removed from the message, got %s", m.String())
	}
}
