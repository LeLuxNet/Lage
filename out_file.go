package lage

import (
	"fmt"
	"io"
	"os"
)

type File struct {
	Path string

	file io.WriteCloser
}

func (o File) Log(m *Message) {
	if o.file == nil {
		var err error
		o.file, err = os.OpenFile(o.Path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0660)
		if err != nil {
			return
		}
	}

	fmt.Fprintln(o.file, m.String())
}

func (o File) Close() {
	o.file.Close()
}
