package lage

import (
	"strconv"
	"strings"
)

type Color uint8

// https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit

const (
	Reset Color = iota
	Bold
	Faint
	Italic
	Underline
	BlinkSlow
	BlinkRapid
	Invert
	Conceal
	Strike
)

const (
	FBlack Color = iota + 30
	FRed
	FGreen
	FYellow
	FBlue
	FMagenta
	FCyan
	FWhite
)

const (
	FBrBlack Color = iota + 90
	FBrRed
	FBrGreen
	FBrYellow
	FBrBlue
	FBrMagenta
	FBrCyan
	FBrWhite
)

const (
	BBlack Color = iota + 40
	BRed
	BGreen
	BYellow
	BBlue
	BMagenta
	BCyan
	BWhite
)

const (
	BBrBlack Color = iota + 100
	BBrRed
	BBrGreen
	BBrYellow
	BBrBlue
	BBrMagenta
	BBrCyan
	BBrWhite
)

func ansiLen(colors ...Color) int {
	i := 3 + len(colors) - 1
	for _, c := range colors {
		if c < 10 {
			i += 1
		} else if c < 100 {
			i += 2
		} else {
			i += 3
		}
	}
	return i
}

func ansiWrite(b *strings.Builder, colors ...Color) string {
	b.WriteString("\x1b[")
	b.WriteString(strconv.Itoa(int(colors[0])))
	for _, c := range colors[1:] {
		b.WriteByte(';')
		b.WriteString(strconv.Itoa(int(c)))
	}
	b.WriteByte('m')

	return b.String()
}

func ANSI(colors ...Color) string {
	var b strings.Builder
	b.Grow(ansiLen(colors...))
	ansiWrite(&b, colors...)
	return b.String()
}
