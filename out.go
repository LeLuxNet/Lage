package lage

import (
	"github.com/mitchellh/mapstructure"
	"gopkg.in/yaml.v3"
)

type Type interface {
	Log(*Message)
	Close()
}

type Output struct {
	TypeString string
	Type       Type
	Ignore     map[string]struct{}
	Only       map[string]struct{}
}

type outputIn struct {
	Type    string
	Options map[string]interface{}
	Ignore  []string
	Only    []string
}

type outputOut struct {
	Type    string
	Options Type
	Ignore  []string `yaml:",omitempty"`
	Only    []string `yaml:",omitempty"`
}

func (o *Output) UnmarshalYAML(n *yaml.Node) error {
	oi := &outputIn{}
	err := n.Decode(oi)
	if err != nil {
		return err
	}

	o.Type = outputs[oi.Type]()
	err = mapstructure.Decode(&oi.Options, &o.Type)
	if err != nil {
		return err
	}

	o.TypeString = oi.Type
	if oi.Ignore != nil {
		o.Ignore = make(map[string]struct{}, len(oi.Ignore))
		for _, id := range oi.Ignore {
			o.Ignore[id] = struct{}{}
		}
	} else if oi.Only != nil {
		o.Only = make(map[string]struct{}, len(oi.Only))
		for _, id := range oi.Only {
			o.Only[id] = struct{}{}
		}
	}

	return nil
}

func (o Output) MarshalYAML() (interface{}, error) {
	oo := outputOut{}
	oo.Type = o.TypeString
	oo.Options = o.Type
	if o.Ignore != nil {
		oo.Ignore = []string{}
		for id := range o.Ignore {
			oo.Ignore = append(oo.Ignore, id)
		}
	} else if o.Only != nil {
		oo.Only = []string{}
		for id := range o.Only {
			oo.Only = append(oo.Only, id)
		}
	}
	return oo, nil
}

var outputs = map[string](func() Type){
	"stdout": func() Type { return Stdout },
	"stderr": func() Type { return Stderr },
	"file":   func() Type { return File{} },
	"system": func() Type { return NewSystem() },
}

func RegisterType(key string, fn func() Type) {
	outputs[key] = fn
}
