package lage_test

import (
	"testing"

	"lelux.net/lage"
)

func BenchmarkLogSyslog(b *testing.B) {
	l := lage.Logger{
		Outputs: []lage.Output{
			{Type: lage.NewSystem()},
		},
	}

	for i := 0; i < b.N; i++ {
		l.Log(lage.Info, "a", "b", "c")
	}
}
