package lage

func NewDefault() *Logger {
	return &Logger{
		Outputs: []Output{
			{
				TypeString: "stdout",
				Type:       Stdout,
				Ignore: map[string]struct{}{
					Error.ID: {},
					Fatal.ID: {},
				},
			},
			{
				TypeString: "stderr",
				Type:       Stderr,
				Only: map[string]struct{}{
					Error.ID: {},
					Fatal.ID: {},
				},
			},
		},
	}
}
