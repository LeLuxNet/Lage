package lage

type SyslogLevel uint8

const (
	SyslogEmergency SyslogLevel = iota + 1
	SyslogAlert
	SyslogCritical
	SyslogError
	SyslogWarning
	SyslogNotice
	SyslogInfo
	SyslogDebug
)
