package lage

import (
	"fmt"
	"time"
)

type Logger struct {
	Name string `yaml:"-"`

	parent   *Logger
	children map[*Logger]struct{}

	Outputs []Output
}

func (l *Logger) Parent() *Logger {
	return l.parent
}

func (l *Logger) SetParent(p *Logger) {
	if l.parent != nil {
		delete(l.parent.children, l)
	}

	l.parent = p
	if p == nil {
		return
	}

	if p.children == nil {
		p.children = make(map[*Logger]struct{})
	}
	p.children[l] = struct{}{}
}

func (l *Logger) Log(lvl *Level, data ...interface{}) {
	if l != nil {
		c := fmt.Sprintln(data...)
		l.log(lvl, c[:len(c)-1])
	}
}

func (l *Logger) Logf(lvl *Level, format string, data ...interface{}) {
	if l != nil {
		l.log(lvl, fmt.Sprintf(format, data...))
	}
}

func (l *Logger) log(lvl *Level, c string) {
	if l.parent == nil && len(l.Outputs) == 0 {
		return
	}

	var names []string

	p := l
	for p != nil {
		if p.Name != "" {
			names = append(names, p.Name)
		}
		p = p.parent
	}

	last := len(names) - 1
	for i := 0; i < len(names)/2; i++ {
		names[i], names[last-i] = names[last-i], names[i]
	}

	m := &Message{
		Level:   lvl,
		Names:   names,
		Content: c,
		Time:    time.Now(),
	}

	l.logMsg(m)
}

func (l Logger) logMsg(m *Message) {
	if l.parent != nil {
		l.parent.logMsg(m)
	}
	for _, o := range l.Outputs {
		if o.Ignore != nil {
			if _, ok := o.Ignore[m.Level.ID]; ok {
				continue
			}
		} else if o.Only != nil {
			if _, ok := o.Only[m.Level.ID]; !ok {
				continue
			}
		}
		o.Type.Log(m)
	}
}

func (l *Logger) Close() {
	if l == nil {
		return
	}

	for _, o := range l.Outputs {
		o.Type.Close()
	}
	for c := range l.children {
		c.Close()
	}
}
