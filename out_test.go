package lage_test

import (
	"io"
	"testing"

	"lelux.net/lage"
)

func BenchmarkLogColor(b *testing.B) {
	l := lage.Logger{
		Outputs: []lage.Output{
			{Type: lage.Writer{
				Writer: io.Discard,
				Color:  true,
			}},
		},
	}

	for i := 0; i < b.N; i++ {
		l.Log(lage.Info, "a", "b", "c")
	}
}

func BenchmarkLogNoColor(b *testing.B) {
	l := lage.Logger{
		Outputs: []lage.Output{
			{Type: lage.Writer{
				Writer: io.Discard,
			}},
		},
	}

	for i := 0; i < b.N; i++ {
		l.Log(lage.Info, "a", "b", "c")
	}
}
