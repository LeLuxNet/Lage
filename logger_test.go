package lage_test

import (
	"testing"

	"lelux.net/lage"
)

func TestNilLogger(t *testing.T) {
	var l *lage.Logger
	l.Log(lage.Info, "abc")
}
