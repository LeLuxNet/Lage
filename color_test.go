package lage_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"lelux.net/lage"
)

func TestANSI(t *testing.T) {
	assert.Equal(t, "\x1b[34m", lage.ANSI(lage.FBlue))

	assert.Equal(t, "\x1b[41;97m", lage.ANSI(lage.BRed, lage.FBrWhite))
}

func BenchmarkANSI(b *testing.B) {
	for i := 0; i < b.N; i++ {
		lage.ANSI(lage.Bold, lage.Italic, lage.Underline, lage.Strike, lage.FBlack, lage.BWhite)
	}
}
