package lage

import (
	"fmt"
	"io"
	"os"
)

var (
	Stdout = Writer{Writer: os.Stdout, Color: true}
	Stderr = Writer{Writer: os.Stderr, Color: true}
)

type Writer struct {
	Writer io.Writer `yaml:"-"`
	Color  bool
}

func (o Writer) Log(m *Message) {
	if o.Color {
		fmt.Fprintln(o.Writer, m.StringANSI())
	} else {
		fmt.Fprintln(o.Writer, m.String())
	}
}

func (o Writer) Close() {}
