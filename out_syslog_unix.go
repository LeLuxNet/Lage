//go:build !windows
// +build !windows

package lage

import (
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

func NewSystem() System {
	for _, network := range []string{"unixgram", "unix"} {
		for _, address := range []string{"/dev/log", "/var/run/syslog", "/var/run/log"} {
			conn, err := net.Dial(network, address)
			if err == nil {
				return System{conn: conn}
			}
		}
	}

	return System{}
}

func (o System) Log(m *Message) {
	if o.conn != nil {
		lvl := m.Level.SyslogLevel

		name := o.Name
		if name == "" {
			name = os.Args[0]
		}

		pid := strconv.Itoa(os.Getpid())

		m.filterANSI()

		i := 1 + 1 + 1 +
			len(time.Stamp) + 1 +
			len(name) +
			1 + len(pid) + 3 +
			m.lenJoinedNames() +
			len(m.contentNoANSI) + 1

		if lvl == 0 {
			lvl = SyslogInfo
			i += 1 + len(m.Level.Name) + 2
		}

		var b strings.Builder
		b.Grow(i)

		b.WriteByte('<')
		b.WriteString(strconv.Itoa(int(lvl) - 1))
		b.WriteByte('>')

		b.WriteString(m.Time.Format(time.Stamp))
		b.WriteByte(' ')

		b.WriteString(name)

		b.WriteByte('[')
		b.WriteString(pid)
		b.WriteString("]: ")

		if lvl == 0 {
			b.WriteByte('<')
			b.WriteString(m.Level.Name)
			b.WriteString("> ")
		}

		m.writeJoinedNames(&b)
		b.WriteString(m.contentNoANSI)
		b.WriteByte('\n')

		o.conn.Write([]byte(b.String()))
	}
}

func (o System) Close() {
	if o.conn != nil {
		o.conn.Close()
	}
}
